#include "alUtil.h"
#include <ai.h>

AI_SHADER_NODE_EXPORT_METHODS(alVectorUtilMtd)

enum alVectorUtilParams
{
	p_input1,
	p_input2,
	p_operation
};

enum UtilEnum
{
	U_LENGHT=0,
	U_DOT,
	U_CROSS,
	U_NORMALIZE
};

static const char* UtilNames[] =
{
	"length(1)",
	"dot(1,2)",
	"cross(1,2)",
	"normalize(1)",
	NULL
};

node_parameters
{
	AiParameterVec("input1", 1.0f, 1.0f, 1.0f);
	AiParameterVec("input2", 1.0f, 1.0f, 1.0f);
	AiParameterEnum("Operation", 0, UtilNames);
}

node_loader
{
   if (i>0) return 0;
   node->methods     = alVectorUtilMtd;
   node->output_type = AI_TYPE_VECTOR;
   node->name        = "alVectorUtil";
   node->node_type   = AI_NODE_SHADER;
   strcpy(node->version, AI_VERSION);
   return true;
}

node_initialize
{

}

node_finish
{

}

node_update
{

}

shader_evaluate
{
	AtVector vector1;
	AtVector vector2;
	AtVector vresult;
	int operation = AiShaderEvalParamInt(p_operation);

	switch(operation)
	{
	case U_LENGHT:
        vector1 = AiShaderEvalParamVec(p_input1);
		vresult = AiV3Length(vector1);
		break;
	case U_DOT:
   		vector1 = AiShaderEvalParamVec(p_input1);
		vector2 = AiShaderEvalParamVec(p_input2);
		vresult = AiV3Dot(vector1, vector2);
		break;
	case U_CROSS:
		vector1 = AiShaderEvalParamVec(p_input1);
		vector2 = AiShaderEvalParamVec(p_input2);
		vresult = AiV3Cross(vector1, vector2);
		break;
	case U_NORMALIZE:
		vector1 = AiShaderEvalParamVec(p_input1);
		vresult = AiV3Normalize(vector1);
		break;
	default:
		vresult = vector1;
		break;
	}

	sg->out.VEC() = vresult;
}


