#include "alUtil.h"
#include "Remap.h"
#include <ai.h>

AI_SHADER_NODE_EXPORT_METHODS(alTensionMtd)

enum OutputEnum
{
    NS_SQUASH=0,
    NS_STRETCH,
    NS_BOTH
};

static const char* outputNames[] = 
{
    "squash",
    "stretch",
    "both",
    NULL
};

enum alVectorUtilParams
{
    p_output,
    REMAP_FLOAT_PARAM_ENUM
};


node_parameters
{
    AiParameterEnum("output", 0, outputNames);
    REMAP_FLOAT_PARAM_DECLARE;
}

node_loader
{
   if (i>0) return 0;
   node->methods     = alTensionMtd;
   node->output_type = AI_TYPE_RGB;
   node->name        = "alTension";
   node->node_type   = AI_NODE_SHADER;
   strcpy(node->version, AI_VERSION);
   return true;
}

node_initialize
{

}

node_finish
{

}

node_update
{

}

shader_evaluate
{

    int mode = AiShaderEvalParamInt(p_output);

    AtVector pref_dx = {0, 0, 0};
    AtVector pref_dy = {0, 0, 0};
    
    static AtString str_Pref("Pref");
    AiUDataGetDxyDerivativesVec(str_Pref, pref_dx, pref_dy);
    
    float det = sg->dudx*sg->dvdy - sg->dudy*sg->dvdx;

    AtVector pref_du = (pref_dx * sg->dvdy - pref_dy * sg->dvdx) / det;
    AtVector pref_dv = (pref_dy * sg->dudx - pref_dx * sg->dudy) / det;

    float PrefLength = AiV3Length(pref_du) + AiV3Length(pref_dv);
	float PLength = AiV3Length(sg->dPdu) + AiV3Length(sg->dPdv);
    
    AtRGB result = {0, 0, 0};

    float squash = 0;
    float stretch = 0;

     if (PLength != 0. && PrefLength != 0.){
        if(PrefLength != PLength){
            squash = (PrefLength / PLength) - 1.;
            stretch = (PLength / PrefLength) - 1.;
        }
    }
    
    switch(mode){
        case NS_SQUASH:
        result = squash;
        break;
        case NS_STRETCH:
        result = stretch;
        break;
        case NS_BOTH:
        result[0] = squash;
        result[1] = stretch;
        break;
    }

   
    RemapFloat r = REMAP_FLOAT_CREATE;
    result.r = r.remap(result.r);
    result.g = r.remap(result.g);
    result.b = r.remap(result.b);


	sg->out.RGB() = result;
}


