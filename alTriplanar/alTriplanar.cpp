#include "Remap.h"
#include <string.h>
#include <sstream>
#include <ai.h>

#define CELLSAMPLES 3

AI_SHADER_NODE_EXPORT_METHODS(alTriplanar)

enum TriplanarSpaceEnum
{
    NS_WORLD = 0,
    NS_OBJECT,
    NS_PREF,
    NS_PREF_NO_NREF,
};

static const char* triplanarSpaceNames[] =
{
    "world",
    "object",
    "Pref",
    "PrefNoNref",
    NULL
};

enum TriplanarTiling
{
    TM_REGULAR = 0,
    TM_CELLNOISE
};

static const char* triplanarTilingNames[] =
{
    "regular",
    "cellnoise",
    NULL
};

enum alTriplanarParams
{
    p_space,

    p_tiling,

    p_frequency,
    p_frequencyX,
    p_frequencyY,
    p_frequencyZ,

    p_texture,
    p_textureXEnable,
    p_textureYEnable,
    p_textureZEnable,
    p_textureX,
    p_textureY,
    p_textureZ,

    p_frame,
    p_frameX,
    p_frameY,
    p_frameZ,

    p_mipmapbias,
    p_blendSoftness,
    p_cellSoftness,
    p_scaleux,
    p_scalevx,
    p_scaleuy,
    p_scalevy,
    p_scaleuz,
    p_scalevz,
    p_offsetux,
    p_offsetvx,
    p_offsetuy,
    p_offsetvy,
    p_offsetuz,
    p_offsetvz,
    p_rotx,
    p_roty,
    p_rotz,
    p_rotjitterx,
    p_rotjittery,
    p_rotjitterz
};

node_parameters
{
    AiParameterEnum("space", 0, triplanarSpaceNames);

    AiParameterEnum("tiling", 0, triplanarTilingNames);

    AiParameterFlt("frequency", 1.0f);
    AiParameterFlt("frequencyX", 1.0f);
    AiParameterFlt("frequencyY", 1.0f);
    AiParameterFlt("frequencyZ", 1.0f);

    AiParameterStr("texture", "");
    AiParameterBool("textureXEnable", false);
    AiParameterBool("textureYEnable", false);    
    AiParameterBool("textureZEnable", false);    

    AiParameterStr("textureX", "");
    AiParameterStr("textureY", "");
    AiParameterStr("textureZ", "");

    AiParameterInt("frame", 0);
    AiParameterInt("frameX", 0);
    AiParameterInt("frameY", 0);
    AiParameterInt("frameZ", 0);

    AiParameterInt("mipmapbias", 0);
    AiParameterFlt("blendSoftness", 0.1);
    AiParameterFlt("cellSoftness", 0.1);  
    AiParameterFlt("scaleux", 1.0f);
    AiParameterFlt("scalevx", 1.0f);
    AiParameterFlt("scaleuy", 1.0f);
    AiParameterFlt("scalevy", 1.0f);
    AiParameterFlt("scaleuz", 1.0f);
    AiParameterFlt("scalevz", 1.0f);
    AiParameterFlt("offsetux", 0.0f);
    AiParameterFlt("offsetvx", 0.0f);
    AiParameterFlt("offsetuy", 0.0f);
    AiParameterFlt("offsetvy", 0.0f);
    AiParameterFlt("offsetuz", 0.0f);
    AiParameterFlt("offsetvz", 0.0f);
    AiParameterFlt("rotx", 0.0f);
    AiParameterFlt("roty", 0.0f);
    AiParameterFlt("rotz", 0.0f);
    AiParameterFlt("rotjitterx", 1.0f);
    AiParameterFlt("rotjittery", 1.0f);
    AiParameterFlt("rotjitterz", 1.0f);
}

node_loader
{
   if (i>0) return 0;
   node->methods     = alTriplanar;
   node->output_type = AI_TYPE_RGB;
   node->name        = "alTriplanar";
   node->node_type   = AI_NODE_SHADER;
   strcpy(node->version, AI_VERSION);
   return true;
}

struct ShaderData
{
    AtTextureHandle *texturehandle[3];
    AtTextureParams *textureparams[3];
};




node_initialize
{
    ShaderData *data = new ShaderData;

    int framEnum = AiNodeGetInt(node, "frame");
    int framEnumXYZ[3] ={framEnum, framEnum, framEnum};

    std::string texname = AiNodeGetStr(node, "texture").c_str();
    std::string texnameXYZ[3] = {texname, texname, texname};    

 // convert integer to four digit string (looks buggy to me...)
    std::stringstream out;
    out << framEnum;
    std::string frameString;
    frameString = out.str();    

    size_t found = texname.find("####");
    if (found!=std::string::npos){
        texname.replace(found,4,frameString);
    }

    std::string frameStringXYZ[3] = {frameString,frameString,frameString};

    for (int i = 0; i < 3; ++i){
        data->texturehandle[i] = AiTextureHandleCreate(texname.c_str());
        data->textureparams[i] = new AtTextureParams;
        AiTextureParamsSetDefaults(*data->textureparams[i]);
        data->textureparams[i]->mipmap_bias = AiNodeGetInt(node, "mipmapbias");
    }



    if(AiNodeGetBool(node, "textureXEnable")){
        framEnumXYZ[0] = AiNodeGetInt(node, "frameX");
        texnameXYZ[0] = AiNodeGetStr(node, "textureX").c_str();
        
     // convert integer to four digit string (looks buggy to me...)
        std::stringstream outX;
        outX << framEnumXYZ[0];
        frameStringXYZ[0] = outX.str();

        size_t foundX = texnameXYZ[0].find("####");
        if (foundX!=std::string::npos){
            texnameXYZ[0].replace(foundX,4,frameStringXYZ[0]);
        }   
        data->texturehandle[0] = AiTextureHandleCreate(texnameXYZ[0].c_str());
    }


    if(AiNodeGetBool(node, "textureYEnable")){
        framEnumXYZ[1] = AiNodeGetInt(node, "frameY");
        texnameXYZ[1] = AiNodeGetStr(node, "textureY").c_str();
        
     // convert integer to four digit string (looks buggy to me...)
        std::stringstream outY;
        outY << framEnumXYZ[1];
        frameStringXYZ[1] = outY.str();

        size_t foundY = texnameXYZ[1].find("####");
        if (foundY!=std::string::npos){
            texnameXYZ[1].replace(foundY,4,frameStringXYZ[1]);
        }
        data->texturehandle[1] = AiTextureHandleCreate(texnameXYZ[1].c_str());
    }


    if(AiNodeGetBool(node, "textureZEnable")){
        framEnumXYZ[2] = AiNodeGetInt(node, "frameZ");
        texnameXYZ[2] = AiNodeGetStr(node, "textureZ").c_str();

     // convert integer to four digit string (looks buggy to me...)
        std::stringstream outZ;
        outZ << framEnumXYZ[2];
        frameStringXYZ[2] = outZ.str();

        size_t foundZ = texnameXYZ[2].find("####");
        if (foundZ!=std::string::npos){
            texnameXYZ[2].replace(foundZ,4,frameStringXYZ[2]);
        }
        data->texturehandle[2] = AiTextureHandleCreate(texnameXYZ[2].c_str());
    }

    AiNodeSetLocalData(node, data);
}

node_finish
{
    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);
    for(int i = 1; i <3; ++i){

        AiTextureHandleDestroy(data->texturehandle[i]);
        delete data->textureparams[i];
    }
    delete data;

}

node_update
{
    ShaderData* data = (ShaderData*)AiNodeGetLocalData(node);
    for(int i = 1; i <3; ++i){
        data->textureparams[i]->mipmap_bias = AiNodeGetInt(node, "mipmapbias");
    }
}

struct projectionTransforms{
    float frequency;
    float frequencyX;
    float frequencyY;
    float frequencyZ;
    float scaleux;
    float scalevx;
    float scaleuy;
    float scalevy;
    float scaleuz;
    float scalevz;
    float offsetux;
    float offsetvx;
    float offsetuy;
    float offsetvy;
    float offsetuz;
    float offsetvz;
    float rotx;
    float rotjitterx;
    float roty;
    float rotjittery;
    float rotz;
    float rotjitterz;
};

struct SGCache{
    
    void initCache(const AtShaderGlobals *sg){
        u = sg->u;
        v = sg->v;
        dudx = sg->dudx;
        dudy = sg->dudy;
        dvdx = sg->dvdx;
        dvdy = sg->dvdy;
    }

    void restoreSG(AtShaderGlobals *sg){
        sg->u = u;
        sg->v = v;
        sg->dudx = dudx;
        sg->dudy = dudy;
        sg->dvdx = dvdx;
        sg->dvdy = dvdy;
    }

    float u;
    float v;
    float dudx;
    float dudy;
    float dvdx;
    float dvdy;
};

void getProjectionGeometry(const AtNode* node, const AtShaderGlobals *sg, int space, AtVector *P, AtVector *N, AtVector *dPdx, AtVector *dPdy){
    AtVector baseN = AiV3Normalize(sg->N);

    AtRGBA PrefCol;
    AtRGBA NrefCol;
    static AtString str_Pref("Pref");
    static AtString str_Nref("Nref");
 
    switch (space)
    {
    case NS_WORLD:
        *P = sg->P;
        *N = baseN;
        *dPdx = sg->dPdx;
        *dPdy = sg->dPdy;
        break;
    case NS_OBJECT:
        *P = sg->Po;
        if (sg->sc == AI_CONTEXT_DISPLACEMENT){
            // normals are already in object space during displacement
            *N = baseN;
        } else {
            *N = AiShaderGlobalsTransformNormal(sg, baseN, AI_WORLD_TO_OBJECT);
        }
        *dPdx = AiShaderGlobalsTransformVector(sg, sg->dPdx, AI_WORLD_TO_OBJECT);
        *dPdy = AiShaderGlobalsTransformVector(sg, sg->dPdy, AI_WORLD_TO_OBJECT);
        break;
    case NS_PREF:
       if (AiUDataGetRGBA(str_Pref, PrefCol) && AiUDataGetRGBA(str_Nref, NrefCol)){ 
            // Pref and Nref as color sets
            P->x = PrefCol[0];
            P->y = PrefCol[1];
            P->z = PrefCol[2];

            N->x = NrefCol[0];
            N->y = NrefCol[1];
            N->z = NrefCol[2];

            *dPdx = sg->dPdx;
            *dPdy = sg->dPdy;
        } else {
            if (!AiUDataGetVec(str_Pref, *P)) {
                // No Pref, so falling back to Object space P
                //AiMsgWarning("alTriplanar could not get Pref");
                *P = sg->Po;
                if (sg->sc == AI_CONTEXT_DISPLACEMENT){
                    // normals are already in object space during displacement
                    *N = baseN;
                } else {
                    *N = AiShaderGlobalsTransformNormal(sg, baseN, AI_WORLD_TO_OBJECT);
                }
                *dPdx = AiShaderGlobalsTransformVector(sg, sg->dPdx,
                                                       AI_WORLD_TO_OBJECT);
                *dPdy = AiShaderGlobalsTransformVector(sg, sg->dPdy,
                                                       AI_WORLD_TO_OBJECT);
            } else {
                // Pref and Nref from vector attributes
                *dPdx = sg->dPdx;
                *dPdy = sg->dPdy;
                if(!AiUDataGetVec(str_Nref, *N)){
                    //AiMsgError("alTriplanar could not get Nref");
                }
            }
            }
       break;
   case NS_PREF_NO_NREF:
      if (!AiUDataGetVec(str_Pref, *P)) {
           //AiMsgError("alTriplanar could not get Pref");
       } else {
            // Pref from vector attribute - Nref is computed
            if(!AiUDataGetVec(str_Nref, *N)){
                AiUDataGetDxyDerivativesVec(str_Pref, *dPdx, *dPdy);
                *N = AiV3Normalize(AiV3Cross(AiV3Normalize(*dPdx), AiV3Normalize(*dPdy)));
            }
            else {
                *dPdx = sg->dPdx;
                *dPdy = sg->dPdy;
            }
       }
       break;
    default:
        *P = sg->P;
        *N = baseN;
        *dPdx = sg->dPdx;
        *dPdy = sg->dPdy;
        break;
    }
}

void computeBlendWeights(const AtVector N, float blendSoftness, float *weights){
    weights[0] = fabsf(AiV3Normalize(N).x);
    weights[1] = fabsf(AiV3Normalize(N).y);
    weights[2] = fabsf(AiV3Normalize(N).z);
    float weightsum = 0.f;
    for(int i=0; i<3; ++i){
        weights[i] = weights[i] - (1.f-blendSoftness)/2.f;
        weights[i] = AiMax(weights[i], 0.00f);
        weightsum += weights[i];
    }
    if(weightsum){
        for(int i=0; i<3; ++i){
            weights[i] /= weightsum;
        }
    }
}

inline void rotateUVs(AtVector &P, float degrees){
    AtVector orientVectorX;
    const double d2r = 1. / 360. * AI_PI * 2;
    double phi = d2r * degrees;
    orientVectorX.x = cosf(phi);
    orientVectorX.y = sinf(phi);
    orientVectorX.z = 0.f;

    AtVector orientVectorZ;
    orientVectorZ.x = 0.f;
    orientVectorZ.y = 0.f;
    orientVectorZ.z = 1.f;

    AtVector orientVectorY = AiV3Cross(orientVectorX, orientVectorZ);

    AiV3RotateToFrame(P, orientVectorX, orientVectorY, orientVectorZ);
}

inline AtRGBA tileRegular(const AtVector &P, const AtVector &dPdx, const AtVector dPdy,
                          const projectionTransforms &pt,
                          float *weights, AtShaderGlobals *sg,
                          AtTextureHandle *handle[], AtTextureParams *params[]){
    AtRGBA lookups[3];
    bool textureAccessX = true;
    bool textureAccessY = true;
    bool textureAccessZ = true;

    // lookup X
    AtVector ProjP;
    ProjP.x = (P.z + pt.offsetux) * pt.frequencyX / pt.scaleux ;
    ProjP.y = (-P.y + pt.offsetvx) * pt.frequencyX / pt.scalevx;
    ProjP.z = 0.;
    rotateUVs(ProjP, pt.rotx);

    sg->u = ProjP.x;
    sg->v = ProjP.y;
    sg->dudx = dPdx.z * pt.frequencyX / pt.scaleux;
    sg->dudy = dPdy.z * pt.frequencyX / pt.scaleux;
    sg->dvdx = dPdx.y * pt.frequencyX / pt.scalevx;
    sg->dvdy = dPdy.y * pt.frequencyX / pt.scalevx;

    if(weights[0] > 0.001){
        lookups[0] = AiTextureHandleAccess(sg, handle[0], *params[0], &textureAccessX);
    } else {
        lookups[0] = AI_RGBA_ZERO;
    }

    // lookup Y
    ProjP.x = (P.x + pt.offsetuy) * pt.frequencyY / pt.scaleuy;
    ProjP.y = (P.z + pt.offsetvy) * pt.frequencyY / pt.scalevy;
    ProjP.z = 0.;
    rotateUVs(ProjP, pt.roty);

    sg->u = ProjP.x;
    sg->v = ProjP.y;
    sg->dudx = dPdx.x * pt.frequencyY / pt.scaleuy;
    sg->dudy = dPdy.x * pt.frequencyY / pt.scaleuy;
    sg->dvdx = dPdx.z * pt.frequencyY / pt.scalevy;
    sg->dvdy = dPdy.z * pt.frequencyY / pt.scalevy;

    if(weights[1] > 0.001){
        lookups[1] = AiTextureHandleAccess(sg, handle[1], *params[1], &textureAccessY);
    } else {
        lookups[1] = AI_RGBA_ZERO;
    }

    // lookup Z
    ProjP.x = (P.x + pt.offsetuz) * pt.frequencyZ / pt.scaleuz;
    ProjP.y = (-P.y + pt.offsetvz) * pt.frequencyZ / pt.scalevz;
    ProjP.z = 0.;
    rotateUVs(ProjP, pt.rotz);

    sg->u = ProjP.x;
    sg->v = ProjP.y;
    sg->dudx = dPdx.x * pt.frequencyZ / pt.scaleuz;
    sg->dudy = dPdy.x * pt.frequencyZ / pt.scaleuz;
    sg->dvdx = dPdx.y * pt.frequencyZ / pt.scalevz;
    sg->dvdy = dPdy.y * pt.frequencyZ / pt.scalevz;

    if(weights[2] > 0.001){
        lookups[2] = AiTextureHandleAccess(sg, handle[2], *params[2], &textureAccessZ);
    } else {
        lookups[2] = AI_RGBA_ZERO;
    }

    if(textureAccessX && textureAccessY && textureAccessZ){
        AtRGBA result = AI_RGBA_ZERO;

        if (weights[0] > 0.001){
            result += lookups[0] * weights[0];
        }

        if (weights[1] > 0.001){
            result += lookups[1] * weights[1];
        }

        if (weights[2] > 0.001){
            result += lookups[2] * weights[2];
        }

        return result;
    }
    else {
        // Something went wrong during lookup.
        // TODO: Log the error
        return AI_RGBA_RED;
    }
}

inline bool lookupCellNoise(float u, float v, float dudx, float dudy, float dvdx, float dvdy,
                            const float cellSoftness, float rot, float rotjitter,
                            AtShaderGlobals *sg, AtTextureHandle *handle,
                            AtTextureParams *params, AtRGBA *textureResult){
    AtVector P;
    P.x = u;
    P.y = v;
    P.z = 0.f;
   
    int samples = (cellSoftness == 0) ? 1 : CELLSAMPLES;
    // run cellnoise
    float weights[CELLSAMPLES];
    float f[CELLSAMPLES];
    AtVector delta[CELLSAMPLES];
    uint32_t id[CELLSAMPLES];
    AiCellular(P, samples, 1, 1.92, 1, f, delta, id);

    if(samples == 1){
        weights[0] = 1.f;
    } else {
        // find closest cell
        float closestDistance = 100000.f;
        float distances[CELLSAMPLES];
        for(int i=0; i<samples; ++i){
            distances[i] = AiV3Length(delta[i]);
            closestDistance = AiMin(distances[i], closestDistance);
        }

        float weightsum = 0.f;
        for(int i=0; i<samples; ++i){
            float diff = distances[i] - closestDistance;
            weights[i] = cellSoftness - diff;
            weights[i] = AiMax(0.f, weights[i]);
            weightsum += weights[i];
        }
        if(weightsum){
            for(int i=0; i<samples; ++i){
                weights[i] /= weightsum;
            }
        }
    }
    
   
    bool success = false;
    *textureResult = AI_RGBA_ZERO;
    sg->dudx = dudx;
    sg->dudy = dudy;
    sg->dvdx = dvdx;
    sg->dvdy = dvdy;
    for(int i=0; i<samples; ++i){
        if(weights[i] > 0.001){
            // pick direction for orientation
            AtVector orientVectorX;
            double jitter = (random(id[i])-0.5) * rotjitter;
            double phi = modulo(rot/360. + jitter, 1.f) * AI_PI * 2.;
            orientVectorX.x = cosf(phi);
            orientVectorX.y = sinf(phi);
            orientVectorX.z = 0.f;

            AtVector orientVectorZ;
            orientVectorZ.x = 0.f;
            orientVectorZ.y = 0.f;
            orientVectorZ.z = 1.f;

            AtVector orientVectorY = AiV3Cross(orientVectorX, orientVectorZ);

            AiV3RotateToFrame(delta[i], orientVectorX, orientVectorY, orientVectorZ);

            // find new uv coordinates, set the center of the cell to be 0.5/0.5;
            sg->u = delta[i].x * 0.75 - 0.5;
            sg->v = delta[i].y * 0.75 - 0.5;

            // texture lookup
            bool currentSuccess = false;

            AtRGBA lookup = AiTextureHandleAccess(sg, handle, *params, &success);

      		*textureResult += lookup * weights[i];

            success |= currentSuccess;
        }
    }

    return success;
 }   

inline AtRGBA tileCellnoise(const AtVector &P, const AtVector &dPdx, const AtVector &dPdy,
                            const projectionTransforms &pt, float *weights, 
                            float cellSoftness, AtShaderGlobals *sg,
                            AtTextureHandle *handle[], AtTextureParams *params[]){
    AtRGBA textureResult[3];
	textureResult[0] = AI_RGBA_ZERO;
	textureResult[1] = AI_RGBA_ZERO;
	textureResult[2] = AI_RGBA_ZERO;

    bool textureAccessX = true;
    if(weights[0] > 0.001){
        textureAccessX = lookupCellNoise((P.y + pt.offsetux) * pt.frequencyX / pt.scaleux,
                                          (P.z + pt.offsetvx) * pt.frequencyX / pt.scaleux,
                                          dPdx.y * pt.frequencyX / pt.scaleux,
                                          dPdy.y * pt.frequencyX / pt.scaleux,
                                          dPdx.z * pt.frequencyX / pt.scalevx,
                                          dPdy.z * pt.frequencyX / pt.scalevx,
                                          cellSoftness,
                                          pt.rotx,
                                          pt.rotjitterx,
                                          sg,
                                          handle[0],
                                          params[0],
                                          &textureResult[0]
                                          );
    } else {
        textureResult[0] = AI_RGBA_ZERO;
    }


    bool textureAccessY = true;
    if(weights[1] > 0.001){
        textureAccessY = lookupCellNoise((P.x + pt.offsetuy) * pt.frequencyY / pt.scaleuy,
                                          (P.z + pt.offsetvy) * pt.frequencyY / pt.scalevy,
                                          dPdx.x * pt.frequencyY / pt.scaleuy,
                                          dPdy.x * pt.frequencyY / pt.scaleuy,
                                          dPdx.z * pt.frequencyY / pt.scalevy,
                                          dPdy.z * pt.frequencyY / pt.scalevy,
                                          cellSoftness,
                                          pt.roty,
                                          pt.rotjittery,
                                          sg,
                                          handle[1],
                                          params[1],
                                          &textureResult[1]
                                          );
    } else {
        textureResult[1] = AI_RGBA_ZERO;
    }

    bool textureAccessZ = true;
    if(weights[2] > 0.001){
        textureAccessZ = lookupCellNoise((P.y + pt.offsetuz) * pt.frequencyZ / pt.scaleuz,
                                          (P.x + pt.offsetvz) * pt.frequencyZ / pt.scalevz,
                                          dPdx.y * pt.frequencyZ / pt.scaleuz,
                                          dPdy.y * pt.frequencyZ / pt.scaleuz,
                                          dPdx.x * pt.frequencyZ / pt.scalevz,
                                          dPdy.x * pt.frequencyZ / pt.scalevz,
                                          cellSoftness,
                                          pt.rotz,
                                          pt.rotjitterz,
                                          sg,
                                          handle[2],
                                          params[2],
                                          &textureResult[2]
                                          );
    } else {
        textureResult[2] = AI_RGBA_ZERO;
    }

    if(textureAccessX && textureAccessY && textureAccessZ){
        AtRGBA result = AI_RGBA_ZERO;

        if (weights[0] > 0.001){
            result += textureResult[0] * weights[0];
        }

        if (weights[1] > 0.001){
            result += textureResult[1] * weights[1];
        }

        if (weights[2] > 0.001){
            result += textureResult[2] * weights[2];
        }
        return result;
    }
    else {
        // Something went wrong during lookup.
        // TODO: Log the error
        return AI_RGBA_RED;
    }
}

shader_evaluate
{
    // get shader parameters

//space
    int space = AiShaderEvalParamInt(p_space);

//tiling
    int tiling = AiShaderEvalParamInt(p_tiling);

//frequency
    projectionTransforms pt;

    pt.frequency = AiShaderEvalParamFlt(p_frequency);

    pt.frequencyX = (AiShaderEvalParamBool(p_textureXEnable)) ? AiShaderEvalParamFlt(p_frequencyX) : pt.frequency;
    pt.frequencyY = (AiShaderEvalParamBool(p_textureYEnable)) ? AiShaderEvalParamFlt(p_frequencyY) : pt.frequency;
    pt.frequencyZ = (AiShaderEvalParamBool(p_textureZEnable)) ? AiShaderEvalParamFlt(p_frequencyZ) : pt.frequency;

    float blendSoftness = AiShaderEvalParamFlt(p_blendSoftness);
    blendSoftness = clamp(blendSoftness, 0.f, 1.f);

    float cellSoftness = AiShaderEvalParamFlt(p_cellSoftness);
    cellSoftness = clamp(cellSoftness, 0.f, 1.f);

    pt.scaleux = AiShaderEvalParamFlt(p_scaleux);
    pt.scalevx = AiShaderEvalParamFlt(p_scalevx);
    pt.scaleuy = AiShaderEvalParamFlt(p_scaleuy);
    pt.scalevy = AiShaderEvalParamFlt(p_scalevy);
    pt.scaleuz = AiShaderEvalParamFlt(p_scaleuz);
    pt.scalevz = AiShaderEvalParamFlt(p_scalevz);

    pt.offsetux = AiShaderEvalParamFlt(p_offsetux);
    pt.offsetvx = AiShaderEvalParamFlt(p_offsetvx);
    pt.offsetuy = AiShaderEvalParamFlt(p_offsetuy);
    pt.offsetvy = AiShaderEvalParamFlt(p_offsetvy);
    pt.offsetuz = AiShaderEvalParamFlt(p_offsetuz);
    pt.offsetvz = AiShaderEvalParamFlt(p_offsetvz);

    pt.rotx = AiShaderEvalParamFlt(p_rotx);
    pt.roty = AiShaderEvalParamFlt(p_roty);
    pt.rotz = AiShaderEvalParamFlt(p_rotz);

    pt.rotjitterx = AiShaderEvalParamFlt(p_rotjitterx);
    pt.rotjittery = AiShaderEvalParamFlt(p_rotjittery);
    pt.rotjitterz = AiShaderEvalParamFlt(p_rotjitterz);

        // get local data
    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);

        // set up P and blend weights
    AtVector P;
    AtVector N;
    AtVector dPdx;
    AtVector dPdy;
    SGCache SGC;
    SGC.initCache(sg);

    getProjectionGeometry(node, sg, space, &P, &N, &dPdx, &dPdy);
    float weights[3];
    computeBlendWeights(N, blendSoftness, weights);

        // compute texture values
    AtRGBA result = AI_RGBA_RED;
    switch(tiling){
        case TM_CELLNOISE:
            result = tileCellnoise(P, dPdx, dPdy, pt, weights, cellSoftness, sg, data->texturehandle, data->textureparams);
            break;
        case TM_REGULAR:
            result = tileRegular(P, dPdx, dPdy, pt, weights, sg, data->texturehandle, data->textureparams);
            break;
        default:
            // TODO: We should never end up here. Log the error to inform the shader writer.
            result = AI_RGBA_BLUE;
            break;
    }
    sg->out.RGB() = result.rgb();// lerp(input, result.rgb(), result.a);

        // clean up after ourselves
    SGC.restoreSG(sg);
}


