#include <string.h>
#include <sstream>

#include "alUtil.h"
#include <ai.h>

AI_SHADER_NODE_EXPORT_METHODS(alUdimSwitcherMtd)

enum alSwitchParams {
    p_inputA,
    p_inputB,
    p_udimlist
};

node_parameters {
    AiParameterRGB("inputA", 0.f, 0.f, 0.f);
    AiParameterRGB("inputB", 1.f, 1.f, 1.f);
    AiParameterStr("udimList", "1001, 1002, 1003");
}

node_loader {
    if (i > 0)
        return 0;
    node->methods = alUdimSwitcherMtd;
    node->output_type = AI_TYPE_RGB;
    node->name = "alUdimSwitcher";
    node->node_type = AI_NODE_SHADER;
    strcpy(node->version, AI_VERSION);
    return true;
}

node_initialize {}

node_finish {}

node_update {}

bool evalUdimMask(float u, float v, const std::string &udimList){

    int udim = 1000 + ceil(u) + floor(v) * 10;

    std::string udim_str = std::to_string(udim);

    return udimList.find(udim_str) != std::string::npos;
}

shader_evaluate {
    std::string udim_str = AiNodeGetStr(node, "udimList").c_str();

    AtRGB result = AI_RGB_BLACK;

    if(evalUdimMask(sg->u, sg->v, udim_str)) {
        result = AiShaderEvalParamRGB(p_inputB);
    }
    else{
        result = AiShaderEvalParamRGB(p_inputA);
    }

    sg->out.RGB() = result;
}
