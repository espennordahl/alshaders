#include <ai.h>
#include "alUtil.h"
#include <sstream>

AI_SHADER_NODE_EXPORT_METHODS(alWindow)

enum WindowSpaceEnum
{
    NS_WORLD = 0,
    NS_OBJECT,
    NS_PREF,
};

static const char* windowSpaceNames[] =
{
    "world",
    "object",
    "Pref",
    NULL
};

enum alWindowParams
{
	//global control
	p_interiorMap,
    p_numMaps,
    p_roomSpace,
    p_roomSize,
    p_roomOffset,
	p_roomRotation,

	//threshhold value for lights to be entirely on or off per floor basis
	p_lightSwitchFloor,
	p_seed,

	//local control for the floors with lights ON
	p_lightsOnVariation,	
    p_lightsOnMinSaturation,
    p_lightsOnMaxSaturation,
    p_lightsOnMinHueOffset,
    p_lightsOnMaxHueOffset,
    p_lightsOnMinIntensity,
    p_lightsOnMaxIntensity,
	p_lightsOnFloor_bias,
	p_lightsOnFloor_gain,
	

	//local control for the floors with lights OFF
	p_lightsOffVariation,
    p_lightsOffMinSaturation,
    p_lightsOffMaxSaturation,
    p_lightsOffMinHueOffset,
    p_lightsOffMaxHueOffset,
    p_lightsOffMinIntensity,
    p_lightsOffMaxIntensity,	
	p_lightsOffFloor_bias,
	p_lightsOffFloor_gain,


	p_fresnelOn,
};

node_parameters
{
    AiParameterStr("interiorMap", "");
    AiParameterInt("numMaps", 1);
    AiParameterEnum("roomSpace", 1, windowSpaceNames);
    AiParameterVec("roomSize", 1.f, 1.f, 1.f);
    AiParameterVec("roomOffset", 0.001, 0.001, 0.001);
 	AiParameterVec("roomRotation", 0.0, 0.0, 0.0);
	AiParameterFlt("lightSwitchPerFloor", 0.5);
	AiParameterFlt("seed", 1.0);

	AiParameterBool("AddVariationLightsOn", false);    
    AiParameterFlt("minSaturationLightsOn", 0.0);
    AiParameterFlt("maxSaturationLightsOn", 1.0);
    AiParameterFlt("minHueOffsetLightsOn", -.01);
    AiParameterFlt("maxHueOffsetLightsOn", 0.01);
    AiParameterFlt("minIntensityLightsOn", 0);
    AiParameterFlt("maxIntensityLightsOn", 1.f);
	AiParameterFlt("biasLightsOn", 0.5);
	AiParameterFlt("gainLightsOn", 0.5);

	AiParameterBool("AddVariationLightsOff", false);    
    AiParameterFlt("minSaturationLightsOff", 0.0);
    AiParameterFlt("maxSaturationLightsOff", 1.0);
    AiParameterFlt("minHueOffsetLightsOff", -.01);
    AiParameterFlt("maxHueOffsetLightsOff", 0.01);
    AiParameterFlt("minIntensityLightsOff", 0);
    AiParameterFlt("maxIntensityLightsOff", 1.f);
	AiParameterFlt("biasLightsOff", 0.5);
	AiParameterFlt("gainLightsOff", 0.5);

	AiParameterBool("fresnelOn", true);

}

node_loader
{
   if (i>0) return 0;
   node->methods     = alWindow;
   node->output_type = AI_TYPE_RGB;
   node->name        = "alWindow";
   node->node_type   = AI_NODE_SHADER;
   strcpy(node->version, AI_VERSION);
   return true;
}
struct ShaderData
{
    AtTextureHandle *texturehandles[64];
    AtTextureParams *textureparams[64];
    int numTextures;
};

std::string intToString(int number)
{
    std::stringstream ss;
    ss << number;
    return ss.str();
}

const char* parseTextureName(const char *inputString, int textureID){

    std::string outputString = std::string(inputString);
    size_t hashPos = outputString.find("####");
    if(hashPos != std::string::npos){
        std::string number = intToString(textureID);
        while (number.length() < 4) {
            number = "0" + number;
        }
        outputString.replace(hashPos, 4, number);
        return outputString.c_str();     
    }
    else{
        return inputString;
    }
}

node_initialize
{
    ShaderData *data = new ShaderData;
    data->numTextures = std::max(1, AiNodeGetInt(node, "numMaps"));
    for (int i=0; i<data->numTextures; ++i){
        const char *texname = parseTextureName(AiNodeGetStr(node, "interiorMap"), i+1);
        //std::cout << "Texture name: " << texname << std::endl;
        data->texturehandles[i] = AiTextureHandleCreate(texname);
        data->textureparams[i] = new AtTextureParams;
        AiTextureParamsSetDefaults(*data->textureparams[i]);
    }
    AiNodeSetLocalData(node, data);
}

node_finish
{
    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);
    for(int i=0; i<data->numTextures; ++i){
        AiTextureHandleDestroy(data->texturehandles[i]);
        delete data->textureparams[i];
    }
    delete data;
}

node_update
{
}

//vector step
AtVector vectorStep(const AtVector &v1, const AtVector &v2){
	AtVector result;
	for(int i=0; i<3; i++){
		result[i] = (v1[i] < v2[i]) ? 0 : 1;
	}
	return result;
}

float STEP(const float &a, const float &b){
    return a < b ? 0 : 1;
}

AtVector vectorFloor(const AtVector v){
	AtVector result;
	for(int i=0; i<3; i++){
		result[i] = (long long int) v[i] - (long long int) (v[i] < 0);
	}
	return result;
}


float GetBias(float time, float bias)
{
  return (time / ((((1.0/bias) - 2.0)*(1.0 - time))+1.0));
}


float GetGain(float time, float gain)
{
  if(time < 0.5)
    return GetBias(time * 2.0,gain)/2.0;
  else
    return GetBias(time * 2.0 - 1.0,1.0 - gain)/2.0 + 0.5;
}




struct SGCache{
	
	void initCache(const AtShaderGlobals *sg){
		u = sg->u;
		v = sg->v;
		dudx = sg->dudx;
		dudy = sg->dudy;
		dvdx = sg->dvdx;
		dvdy = sg->dvdy;
	}

	void restoreSG(AtShaderGlobals *sg){
		sg->u = u;
		sg->v = v;
		sg->dudx = dudx;
		sg->dudy = dudy;
		sg->dvdx = dvdx;
		sg->dvdy = dvdy;
	}

	float u;
	float v;
	float dudx;
	float dudy;
	float dvdx;
	float dvdy;
};

AtVector2 computeWindowsUVs(AtVector &localP, AtVector &localI, const AtVector &roomOffset, const AtVector &roomRotation, AtVector &roomSize, AtVector &roomID, AtShaderGlobals *sg)
{ 
		//To add offset for each room
//	    localP += roomOffset;
		//To add rotation
//		AtMatrix rotateX = AiM4RotationX(roomRotation[0]);
//		AtMatrix rotateY = AiM4RotationY(roomRotation[1]);
//		AtMatrix rotateZ = AiM4RotationZ(roomRotation[2]);

//		localP = AiM4PointByMatrixMult(rotateY, localP);	

		//To transform normalised ray direction from world space to object space
		//AtVector localI = -AiShaderGlobalsTransformVector(sg, sg->Rd, AI_WORLD_TO_OBJECT);
		//AtVector localI = AiM4VectorByMatrixMult(rotateY, -sg->Rd);
		
		AtVector nextWall = vectorStep(AtVector(0,0,0), localI);

		//To find the intersection point on the wall, height/width/depth defined by roomsize
		//eg. Intersection with the ceiling = ceil(y/d)*d
		//    Intersection with the floor = (ceil(y/d) -1)*d

		AtVector walls = (vectorFloor(localP/roomSize)+nextWall)*roomSize;
		AtVector signs = nextWall*2-1;

		roomID = vectorFloor(localP/roomSize);
		
		AtVector wallOnSurface = AtVector(walls[0] == localP[0], walls[1] == localP[1], walls[2] == localP[2]);
		walls += roomSize*signs*wallOnSurface;

	    AtVector sf = (walls-localP)/localI;
		AtVector intersectionXY = sf[2]*localI+localP;
		AtVector intersectionXZ = sf[1]*localI+localP;
		AtVector intersectionYZ = sf[0]*localI+localP;

		float xVSz = STEP(sf[0],sf[2]);
	    float sf_xVSz = lerp(sf[2], sf[0], xVSz);
		AtVector intersection = lerp(intersectionXY, intersectionYZ, xVSz);
		intersection = lerp(intersectionXZ, intersection, STEP(sf_xVSz, sf[1]));

	    AtVector normalXY = AtVector(0.f, 0.f, 1.f) * (-signs);
	    AtVector normalXZ = AtVector(0.f, 1.f, 0.f) * (-signs);
	    AtVector normalYZ = AtVector(1.f, 0.f, 0.f) * (-signs);

	    AtVector wallNormal = lerp(normalXY, normalYZ, xVSz);
	    wallNormal = lerp(normalXZ, wallNormal, STEP(sf_xVSz, sf[1]));

	    AtVector roomuv = (intersection-(roomID*roomSize))/roomSize;
	    AtVector2 returnUV;
		if(!STEP(sf_xVSz, sf[1])){
	        // Y axis
	        returnUV.x = roomuv[0];
	        returnUV.y = roomuv[2];
	        if(wallNormal[1] > 0.f){
	            // floor
	            returnUV.x = 1-returnUV.x;
	            returnUV.x /= 3.f;
	            returnUV.x += 1.f/3.f;
	            returnUV.y = 1-returnUV.y;
	            returnUV.y /= 3.f;
	        } else {
	            // roof
	            returnUV.x = 1-returnUV.x;
	            returnUV.x /= 3.f;
	            returnUV.x += 1.f/3.f;
	            returnUV.y /= 3.f;
	            returnUV.y += 2.f/3.f; 
	        }
		} else if (xVSz == 1){
	        // X Wall
	        returnUV.x = roomuv[2];
	        returnUV.y = roomuv[1];
	        if(wallNormal[0] > 0.f){
	            // left
	            returnUV.x = 1.f - returnUV.x;
	            returnUV.x /= 3.f;
	            returnUV.y /= 3.f;
	            returnUV.y += 1.f/3.f;
	        } else {
	            // right
	            returnUV.x /= 3.f;
	            returnUV.x += 2.f/3.f;
	            returnUV.y /= 3.f;
	            returnUV.y += 1.f/3.f;
	        }
		} else {
	        // Z Wall
	        returnUV.x = roomuv[0];
	        returnUV.y= roomuv[1];
	        
			if(wallNormal[0] > 0.f){
	            // back
	            returnUV.x /= 3.f;
	            returnUV.x += 1.f/3.f;
	            returnUV.y /= 3.f;
	            returnUV.y += 1.f/3.f;
	        } else {
	            // front 
	            returnUV.x /= 3.f;
	            returnUV.x += 1.f/3.f;
	            returnUV.y /= 3.f;
	            returnUV.y += 1.f/3.f;
	        }		
		}


		return returnUV;
}


void gradeAjustments(float *saturation, float *hueOffset, float *intensity, float *biasVal, float *gainVal, const float minInt, const float maxInt, const float bias, 
	const float gain, const float signal, const float minSat, const float maxSat, const float minHue, const float maxHue, const float minIntensity, const float maxInensity){

	float sat = AiCellNoise2(AtVector2(signal, 51731.132151));
	*saturation = lerp(minSat, maxSat, sat);

	float hue = AiCellNoise2(AtVector2(signal, 173.1231));
	*hueOffset = lerp(minHue, maxHue, hue);

	
    float intens = AiCellNoise2(AtVector2(signal, 413.7254));
    *intensity = lerp(minIntensity, maxInensity, intens);

	float time = AiCellNoise2(AtVector2(signal, 43534.7779));
	*biasVal = GetBias(time, bias);
	*biasVal = std::max(0.1f, *biasVal);
	*gainVal = GetGain(*biasVal, gain);

}



shader_evaluate
{
    
    // get shader parameters
    //global
    int space = AiShaderEvalParamInt(p_roomSpace);
    AtVector roomOffset = AiShaderEvalParamVec(p_roomOffset);
	AtVector roomRotation = AiShaderEvalParamVec(p_roomRotation);
    AtVector roomSize = AiShaderEvalParamVec(p_roomSize);
	float lightThreshHoldFloor = AiShaderEvalParamFlt(p_lightSwitchFloor);
    int numTextures = AiShaderEvalParamInt(p_numMaps);
	bool fresnelOn = AiShaderEvalParamBool(p_fresnelOn);
	float seed = AiShaderEvalParamFlt(p_seed);
	//local
	//controls over floors with lights on
	bool  lightsOnVariation = AiShaderEvalParamBool(p_lightsOnVariation);
    float minSatlightsOn = AiShaderEvalParamFlt(p_lightsOnMinSaturation);
    float maxSatlightsOn = AiShaderEvalParamFlt(p_lightsOnMaxSaturation);
    float minHuelightsOn = AiShaderEvalParamFlt(p_lightsOnMinHueOffset);
    float maxHuelightsOn = AiShaderEvalParamFlt(p_lightsOnMaxHueOffset);
    float minIntensitylightsOn = AiShaderEvalParamFlt(p_lightsOnMinIntensity);
    float maxIntensitylightsOn = AiShaderEvalParamFlt(p_lightsOnMaxIntensity);
	float biaslightsOn = AiShaderEvalParamFlt(p_lightsOnFloor_bias);
	float gainlightsOn = AiShaderEvalParamFlt(p_lightsOnFloor_gain);

	//controls over floors with lights off
	bool  lightsOffVariation = AiShaderEvalParamBool(p_lightsOffVariation);
    float minSatlightsOff = AiShaderEvalParamFlt(p_lightsOffMinSaturation);
    float maxSatlightsOff = AiShaderEvalParamFlt(p_lightsOffMaxSaturation);
    float minHuelightsOff = AiShaderEvalParamFlt(p_lightsOffMinHueOffset);
    float maxHuelightsOff = AiShaderEvalParamFlt(p_lightsOffMaxHueOffset);
    float minIntensitylightsOff = AiShaderEvalParamFlt(p_lightsOffMinIntensity);
    float maxIntensitylightsOff = AiShaderEvalParamFlt(p_lightsOffMaxIntensity);
	float biaslightsOff = AiShaderEvalParamFlt(p_lightsOffFloor_bias);
	float gainlightsOff = AiShaderEvalParamFlt(p_lightsOffFloor_gain);

	AtMatrix rotateX = AiM4RotationX(roomRotation[0]);
	AtMatrix rotateY = AiM4RotationY(roomRotation[1]);
	AtMatrix rotateZ = AiM4RotationZ(roomRotation[2]);

	AtVector localP, roomID, localI;
	AtVector dPdx, dPdy;
    
    // get local data
    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);
    SGCache SGC;
    SGC.initCache(sg);



		
    switch(space){
        case NS_WORLD:
            localP = sg->P;
            dPdx = sg->dPdx;
            dPdy = sg->dPdy;
            localI = AiM4VectorByMatrixMult(rotateY, -sg->Rd);
            break;
        case NS_OBJECT:
        	//std::cout<< "localspacefsdfsdfsadfdf "<< std::endl;	
            localP = sg->Po; // TODO: Tranform derivatives too. See alTriplanar and search for the same thing there..      
            dPdx = AiShaderGlobalsTransformVector(sg, sg->dPdx, AI_WORLD_TO_OBJECT);
            dPdy = AiShaderGlobalsTransformVector(sg, sg->dPdy, AI_WORLD_TO_OBJECT);
            localI = -AiShaderGlobalsTransformVector(sg, sg->Rd, AI_WORLD_TO_OBJECT);
            break;
        case NS_PREF:
            static AtString str_Pref("Pref");
            if (!AiUDataGetVec(str_Pref, localP)){
           		AiMsgError("[alTriplanar] could not get Pref. Using Object space instead");
                localP = sg->Po;
            } else {
            	// Get derivatives for Pref
                AiUDataGetDxyDerivativesVec(str_Pref, dPdx, dPdy);
            }
            break;
        default:
            localP = sg->P;
            dPdx = sg->dPdx;
            dPdy = sg->dPdy;
            break;
            // TODO: Transform derivatives too
    }

	localP += roomOffset;
	localP = AiM4PointByMatrixMult(rotateY, localP);	


    dPdx += localP;
    dPdy += localP;

	//dPdx = sg->dPdx + localP;
	//dPdy = sg->dPdy + localP;







    AtVector2 roomuv = computeWindowsUVs(localP, localI, roomOffset, roomRotation, roomSize, roomID,sg);
    sg -> u = roomuv[0];
    sg -> v = roomuv[1];
	    
    AtVector2 roomuvdX = computeWindowsUVs(dPdx, localI, roomOffset, roomRotation, roomSize, roomID,sg);
    sg -> dudx = roomuv[0] - roomuvdX[0];
 	sg -> dudy = roomuv[1] - roomuvdX[1];

    AtVector2 roomuvdY = computeWindowsUVs(dPdy, localI, roomOffset, roomRotation, roomSize, roomID,sg);
    sg -> dvdx = roomuv[0] - roomuvdY[0];
 	sg -> dvdy = roomuv[1] - roomuvdY[1];


	//randomise texture inputs	
	float signal = AiCellNoise3(roomID);
	signal *= 76.15;
	int textureID = floor(AiCellNoise2(AtVector2(signal, 731.1331)) * numTextures); 
	//make sure textureID is not out of the range    
	textureID = AiMin(textureID, numTextures);

	AtRGB outColor = AI_RGB_BLACK;
    bool textureSuccess = false;

	float floorThreshold = AiCellNoise2(AtVector2(roomID[1], 87873.676*seed));
	float gradientValRoom = 1.0;
	float gradientValFloor = 1.0;
	float biasVal, gainVal, saturation, hueOffset, intensity;
	//float gradientValLightsOn = 1.0f;
	//float gradientValLightOff = 1.0f;


	outColor = AiTextureHandleAccess(sg, data->texturehandles[textureID], *data->textureparams[textureID], &textureSuccess).rgb(); // TODO: Remove

	//std::cout << "lightsOffVariation: " << lightsOffVariation << std::endl;	
	
	if (floorThreshold > lightThreshHoldFloor)// to vary grade adjustements within the floor that have lights off
	{
		

		if (lightsOffVariation)
		{
			//std::cout << "lightsOffVariation: OFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"<< std::endl;	
			gradeAjustments(&saturation, &hueOffset, &intensity, &biasVal, &gainVal, minIntensitylightsOff, maxIntensitylightsOff, biaslightsOff, 
							gainlightsOff, signal, minSatlightsOff, maxSatlightsOff, minHuelightsOff, maxHuelightsOff, minIntensitylightsOff, maxIntensitylightsOff);
			
			if (saturation != 1.0f)
		    {
		        float l = luminance(outColor);
		        outColor = lerp(rgb(l), outColor, saturation);
		    }
		    if (hueOffset != 0.0f)
		    {
		        AtRGB hsv = rgb2hsv(outColor);
		        hsv.r += hueOffset;
		        outColor = hsv2rgb(hsv);
		    }

		    //std::cout << "saturation: " << saturation << std::endl;
		    //std::cout << "gainVal: " << gainVal << std::endl;

		    outColor *= gainVal;
		    outColor *= intensity;

		}else{
				outColor *= 0.0;
		}
		

	}

	else{
			if (lightsOnVariation)
			{
				//std::cout << "lightsOnVariation: ONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"<< std::endl;	
				gradeAjustments(&saturation, &hueOffset, &intensity, &biasVal, &gainVal, minIntensitylightsOn, maxIntensitylightsOn, biaslightsOn, 
								gainlightsOn, signal, minSatlightsOn, maxSatlightsOn, minHuelightsOn, maxHuelightsOn, minIntensitylightsOn, maxIntensitylightsOn);
				if (saturation != 1.0f)
			    {
			        float l = luminance(outColor);
			        outColor = lerp(rgb(l), outColor, saturation);
			    }
			    if (hueOffset != 0.0f)
			    {
			        AtRGB hsv = rgb2hsv(outColor);
			        hsv.r += hueOffset;
			        outColor = hsv2rgb(hsv);
			    }

		    	outColor *= gainVal;
		    	outColor *= intensity;
			}

	}


/*	if (floorThreshold > lightThreshHoldFloor)
		{
			if(lightsOnVariation)
			{
				biasV = gradientValFloor * biasValF;			
				gradientValFloor = GetGain(biasV, floor_gain);
			}
			else{
				gradientValFloor = 0.01;
			}
		} 

	// GRADE ADJUSTMENTS
    float saturation = AiCellNoise2(AtVector2(signal, 51731.132151));

 //   saturation = lerp(minSat, maxSat, saturation);

    if (saturation != 1.0f)
    {
        float l = luminance(outColor);
        outColor = lerp(rgb(l), outColor, saturation);
    }

    float hueOffset = AiCellNoise2(AtVector2(signal, 173.1231));
 //   hueOffset = lerp(minHue, maxHue, hueOffset);
    if (hueOffset != 0.0f)
    {
        AtRGB hsv = rgb2hsv(outColor);
        hsv.r += hueOffset;
        outColor = hsv2rgb(hsv);
    }

    //lIGHTS RANDOMISATION
	float time = AiCellNoise2(AtVector2(signal, 43534.7779));

	//PER FLOOR LIGHTS	
	float biasValF = GetBias(time, floor_bias);
//	biasValF = std::max(0.1f, biasValF);		
	
	//std::cout << "threshold: " << threshold << std::endl;
	

	//PER ROOM LIGHTS
	float biasValR = GetBias(time, room_bias);
	biasValR = std::max(0.1f, biasValR);		
	float roomThreshold = AiCellNoise2(AtVector2(signal, 87873.676));
	if (roomThreshold > lightThreshHoldRoom)
	{
		if(room_distribution)
		{
			biasV = gradientValRoom * biasValR;			
			gradientValRoom = GetGain(biasV, room_gain);
		}
		else{
			gradientValRoom = 0.1;
		}

	} 

	float finalGradient = AiMin(gradientValRoom,gradientValFloor);
  	outColor *= finalGradient;


    float gain = AiCellNoise2(AtVector2(signal, 413.7254));
    gain = lerp(minGain, maxGain, gain);
    outColor *= gain;
*/

	// fresnel
	if (fresnelOn)
	{
	    float costheta = AiV3Dot(AiV3Normalize(sg->Nf), AiV3Normalize(-sg->Rd));
	    outColor *= 1. - (0.05 + (1. - 0.05) * pow((1. - costheta), 5.));
	}

	sg->out.RGB() = outColor;
	//sg->out.RGB() = {1.0, 0.0, 0.0};

    // cleanup
    SGC.restoreSG(sg);
}




